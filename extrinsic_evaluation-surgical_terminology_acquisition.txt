#sentence_ID;masked_sentence;solution
1;Androgen deprivation <mask> is a form of prostate cancer treatment that works by decreasing testosterone levels to castrate levels.;therapy
2;acute urinary <mask> is a common urological emergency.;retention
3;the artificial <mask> sphincter is an implantable device that mimics the biological urinary sphincter by closing during urinary storage with a cuff that opens during voiding.;urinary
4;Renal <mask> carcinoma is a primary tumor with the highest frequency of pancreatic metastasis.;cell
5;undergone a total gastrectomy with Roux-en <mask> reconstruction.;Y
6;Indocyanine <mask> fluorescence imaging.;green
7;<mask> tomography.;computed
8;An end-to-side anastomosis was performed using a <mask> stapler.;circular
9;The insertion hole of the circular stapler was closed using a <mask> stapler.;linear
10;Murphy's <mask> is a maneuver during a physical examination as part of the abdominal examination.;sign
11;coronary artery bypass <mask>.;graft
12;we avoided surgery and selected percutaneous gallbladder <mask> instead.;drainage
13;fistula closure on the duodenal side was performed with the running suture technique using V-<mask>.;loc
14;persistent positive polymerase chain <mask>.;reaction
15;Li-Fraumeni <mask> is a hereditary tumor with autosomal dominant inheritance.;syndrome
16;Surveillance is performed mainly by whole-body <mask> resonance imaging.;magnetic
17;Upper gastrointestinal <mask> is a routine medical emergency.;bleeding
18;peptic ulcer-induced splenic <mask> pseudoaneurysm.;artery
19;The most common cause of acute non-variceal UGIB is peptic ulcer <mask>.;disease
20;resuscitative thoracotomy with aortic cross-<mask>.;clamp
21;lymph <mask> dissection.;node
22;A tumor with intussusception and regional lymph node swelling was located at the <mask> of Treitz.;angle
23;standardized uptake <mask>.;value
24;Lich-Gregoir <mask> is used for the uretero-vesical anastomosis.;technique
25;the last polypropylene suture is used to fix the <mask> -shaped graft to the isthmus.;Y
26;The patient was positioned in the modified Lloyd-Davies <mask>.;position
27;The patient was tilted in the Trendelenburg <mask> by 17 to 20 degrees.;position
28;the liver parenchyma was transected under the Pringle <mask>.;maneuver
29;An Airseal/12-mm <mask> was inserted through the 7th intercostal space.;port
30;The surgeon <mask> the gastrocolic ligament with an ultrasonic scalpel.;separated
31;The fascia in front of the pancreas was cut to the spleen direction to <mask> the splenic artery and vein.;expose
32;The vessels were <mask> and cut off at the root.;clipped
33;The epigastric port is <mask> in the midline.;inserted
34;The fascial defect of the 12 mm infraumbilical port is <mask> up and lifted.;picked
35;local anesthetic was <mask> at the time of surgery.;administered
36;Patients were <mask> in the lithotomy position.;placed
37;Retroperitonealization of the kidney with a peritoneal flap enables graft <mask>.;fixation
38;The peritoneum was <mask> using a 2-0 running barbed, delayed-absorbable suture.;closed
39;The patient was positioned in the modified Lloyd-Davies position and <mask> in the Trendelenburg position by 17 to 20 degrees.;tilted
40;Then, <mask> of the posterior mesorectum proceeded down to the pelvic floor.;mobilization
41;The deep dorsal vein of the penis was divided and <mask> with a robotic vessel sealer.;sealed
42;The deep dorsal vein of the penis was <mask> and sealed with a robotic vessel sealer.;divided
43;Renal damage was <mask> by transecting the ureters at the end of the procedure.;minimized
44;the specimen was <mask> through perineal resection.;retrieved
45;An ileal conduit and ureteric anastomoses were <mask> extracorporeally.;created
46;The bile duct had been <mask> in a duct-to-duct fashion.;reconstructed
47;Afterwards, he <mask> an anastomotic stenosis of the bile duct.;developed
48;Aberrant vessels were <mask> from the chest wall using a vessel sealing device.;separated
49;Next, the surgeon moved to the left side, and the lateral segment of the liver was <mask> instrumentally.;retracted
50;the right gastric artery from the proper hepatic artery was <mask>.;identified
