# SurgicBERTa: A pre-trained language model for procedural surgical language

SurgicBERTa is a language model based on RoBERTa-base (Liu et al., 2019) architecture. We adapted RoBERTa-base to different surgical textbooks and academic papers via continued pretraining. This amount to about 7 million words and 300k surgical sentences. We used the full text of the books and papers in training, not just abstracts. Specific details of the adaptive pretraining procedure and evaluation tasks can be found in the paper below cited.

## Files description
- [This file](https://gitlab.com/altairLab/surgicberta/-/blob/main/extrinsic_evaluation-surgery_anatomy.txt) contains the evaluation sentences used to map the surgery description to the corresponding anatomical target of features. 
- [This file](https://gitlab.com/altairLab/surgicberta/-/blob/main/extrinsic_evaluation-surgical_terminology_acquisition.txt) contains the evaluation sentences used to check if SurgicBERTa has acquired the surgical terminology.
- SurgicBERTa is available on [HuggingFace](https://huggingface.co/marcobombieri/surgicberta).


## Cite us
If you are using SurgicBERTa please cite the following paper:

_[Link will be added soon.]_
